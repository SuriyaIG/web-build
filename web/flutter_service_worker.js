'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "version.json": "f0b2e4846bc0a0d88101e454446d6fba",
"index.html": "1c517a410a50ab1682054c178f07fa2b",
"/": "1c517a410a50ab1682054c178f07fa2b",
"main.dart.js": "7c675fde2224127165494ef29a057295",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"icons/favicon-16x16.png": "4acc09b12b5b9fd024a27dd5b2cdbf3a",
"icons/favicon.ico": "3909e4b1306a7b4113cc306e05666d8d",
"icons/apple-icon.png": "bc31d39bc1b50721e6ddac03392f19c5",
"icons/apple-icon-144x144.png": "8a14c8eff8100e601372ca8cc94610d3",
"icons/android-icon-192x192.png": "d133d2f6cf1ee814c6145e6cc8ca3f25",
"icons/apple-icon-precomposed.png": "bc31d39bc1b50721e6ddac03392f19c5",
"icons/apple-icon-114x114.png": "1f2927b887944041169751a57bc2bf4b",
"icons/ms-icon-310x310.png": "de7051c7ae3aecdd4b479f32a8c00126",
"icons/ms-icon-144x144.png": "8a14c8eff8100e601372ca8cc94610d3",
"icons/apple-icon-57x57.png": "21d12f982de8adc29ef75ea7077990c8",
"icons/apple-icon-152x152.png": "480583c90ef728d1cdf14140d2fed9ca",
"icons/ms-icon-150x150.png": "e453ef92dd9afefcde2c81edcbce6106",
"icons/android-icon-72x72.png": "f7cd8eb161c4c8bbead971d55adb7b24",
"icons/android-icon-96x96.png": "5d1cd8961b0d3a0282cc46c03f2db699",
"icons/android-icon-36x36.png": "463de5657a03a6075e8412542719d791",
"icons/apple-icon-180x180.png": "61a075ebe0d12df0b5ca9a93185cc8c6",
"icons/favicon-96x96.png": "5d1cd8961b0d3a0282cc46c03f2db699",
"icons/android-icon-48x48.png": "ae85a53a677c8725e7ecc852f6d907a0",
"icons/apple-icon-76x76.png": "93e1d69a4dd4ec51e2fe6444e8591196",
"icons/apple-icon-60x60.png": "9d75964b26d4f99191500f5c394f283a",
"icons/browserconfig.xml": "653d077300a12f09a69caeea7a8947f8",
"icons/android-icon-144x144.png": "8a14c8eff8100e601372ca8cc94610d3",
"icons/apple-icon-72x72.png": "f7cd8eb161c4c8bbead971d55adb7b24",
"icons/apple-icon-120x120.png": "a2b3497b608fcade7e5bf6063371e477",
"icons/favicon-32x32.png": "81502afaa5f3b7362f323053843f0afe",
"icons/ms-icon-70x70.png": "8d5374b23086b9bf2ab6080abafad141",
"manifest.json": "bf1dc833ca5ba3c56f09d2e004998132",
"assets/AssetManifest.json": "64f5d9b8b43a9272bfe4b04619a7a4f3",
"assets/NOTICES": "cc320765866bc2be32d05aefb865db56",
"assets/FontManifest.json": "7ded17070490486594844c5b4aabde39",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "6d342eb68f170c97609e9da345464e5e",
"assets/packages/fluttertoast/assets/toastify.js": "e7006a0a033d834ef9414d48db3be6fc",
"assets/packages/fluttertoast/assets/toastify.css": "a85675050054f179444bc5ad70ffc635",
"assets/packages/wakelock_web/assets/no_sleep.js": "7748a45cd593f33280669b29c2c8919a",
"assets/fonts/MaterialIcons-Regular.otf": "4e6447691c9509f7acdbf8a931a85ca1",
"assets/assets/images/skclogo.png": "45d0dc7e104a944c081bc9be906b2a68",
"assets/assets/images/skclogo.jpg": "ebaea90f2265ddb6ddc026f319730a11",
"assets/assets/images/banner.jpg": "4b3093f723c51d296960f5734dba82eb",
"assets/assets/images/skcbglogo.jpg": "44fe4e95c8a23afd85cb4cfdf025b03f",
"assets/assets/images/centerlogo.png": "56ecaff929f7ff547b0b8d5a0c0c9b04",
"assets/assets/images/splashbg.jpg": "d882ec6c833c9cd2756b1d0e4e633916",
"assets/assets/images/bgimage.jpeg": "6522a8b6ee74bffcc76ce40da0743a47",
"assets/assets/images/profilebg.png": "e38641e978d46b26b2684752a7b29c89",
"assets/assets/icons/skctransparent.png": "de8dd88177e4e32e7aaa1832c2a56097",
"assets/assets/icons/dashboard.png": "c45f46a3167e57b031bcec4ee54b4047",
"assets/assets/icons/steth.png": "b3ceca0c1ddac2988a21a6ab132ab44c",
"assets/assets/icons/export.png": "b1449dc34fc2b50b8da519e841b38c89",
"assets/assets/icons/logout.png": "eee76d9a276f007ebc4bb18376a3c092",
"assets/assets/icons/home.png": "95c23e9d9a155efa3fa05d36e0286299",
"assets/assets/icons/user.png": "c98f4d15c5fd3597880eeef1f1c2272d",
"assets/assets/icons/exportpurp.png": "a1b282d77404e35ca6fad98c0d48b74f",
"assets/assets/icons/manageSettings.png": "54884989795afd8c49517181ae0163d2",
"assets/assets/icons/ribbon.png": "9fb7b6245e78d82e9afb5ac1822121fc",
"assets/assets/icons/film.png": "a75925417ea558ed8238196b6cf70dd1",
"assets/assets/icons/question-sequence.png": "fd7afb7a1d69a390548b8bb5413d5299",
"assets/assets/icons/fileupload.png": "204c1eb4839950d986a2d3d69cda4299",
"assets/assets/icons/microscope.png": "c761a2d2d17b359ce53c1f44df0e2932",
"assets/assets/icons/resources.png": "c064ebc858044aa20f3a9b9cedb424d8",
"assets/assets/icons/patient-management.png": "e9d051315acd80c8de2856d488b6af93",
"assets/assets/icons/uploadIcon.png": "d38c6296048fb2743d4a95b208eebc59",
"assets/assets/icons/resource.png": "544349bd197bb7c19c654f414fc7c78f",
"assets/assets/icons/patient-profile.png": "dbfc06eba09f7da528866ce9178f7c87",
"assets/assets/icons/questionmeet.png": "75c4ebb3231f637ed95f4fadcfda5b61",
"assets/assets/icons/report.png": "8f2fc1faf72e284e8a7a9629cb87df0b",
"assets/assets/fonts/Poppins-Medium.ttf": "f61a4eb27371b7453bf5b12ab3648b9e",
"assets/assets/fonts/Poppins-Regular.ttf": "8b6af8e5e8324edfd77af8b3b35d7f9c",
"assets/assets/fonts/Montserrat-Regular.ttf": "ee6539921d713482b8ccd4d0d23961bb"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "/",
"main.dart.js",
"index.html",
"assets/NOTICES",
"assets/AssetManifest.json",
"assets/FontManifest.json"];
// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache.
        return response || fetch(event.request).then((response) => {
          cache.put(event.request, response.clone());
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}

// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
